#! python3

# A simple 12bit computer visualization

### set memory location to start executing ###

counter = 0

# reset register A

a = 0

# reset carry flag

c = 0

# reset RAM

m = []

for i in range(256):
    m.append([])

### sample programs ###

# print the fibonacci sequence up to 233, then start again

m[0] =  ['LDI',  1]
m[1] =  ['STA', 14]
m[2] =  ['LDI',  0]
m[3] =  ['ADD', 14]
m[4] =  ['STA', 15]
m[5] =  ['LDA', 14]
m[6] =  ['OUT',  0]
m[7] =  ['STA', 13]
m[8] =  ['LDA', 15]
m[9] =  ['STA', 14]
m[10] = ['LDA', 13]
m[11] = ['JC',   0]
m[12] = ['JMP',  3]
m[13] = [0,      0]
m[14] = [0,      0]
m[15] = [0,      0]

# multiply two integers in memory location 29 and 30

m[16] = ['LDA', 27] # d = d + b
m[17] = ['ADD', 29]
m[18] = ['STA', 27]
m[19] = ['LDA', 30] # a = a - c
m[20] = ['SUB', 28]
m[21] = ['STA', 30]
m[22] = ['JZ',  24] # if a == 0: goto memory location 24
m[23] = ['JMP', 16] # else: continue
m[24] = ['LDA', 27] # print d
m[25] = ['OUT',  0]
m[26] = ['HLT',  0] # break
m[27] = [0,      0] # d
m[28] = [0,      1] # c
m[29] = [0,      5] # b
m[30] = [0,      6] # a

# divide an integer in memory location 53 by an integer in memory location 52

m[31] = ['LDA', 50] # d = d + c
m[32] = ['ADD', 51]
m[33] = ['STA', 50]
m[34] = ['LDA', 53] # a = a - b
m[35] = ['SUB', 52]
m[36] = ['STA', 53]
m[37] = ['JZ',  40] # if a == 0: goto memory location 40
m[38] = ['JC',  43] # if a < 0: goto memory location 43
m[39] = ['JMP', 31] # else: continue
m[40] = ['LDA', 50] # print d
m[41] = ['OUT',  0]
m[42] = ['HLT',  0] # break
m[43] = ['LDA', 50] # print d - c (= a // b)
m[44] = ['SUB', 51]
m[45] = ['OUT',  0]
m[46] = ['LDA', 53] # print a + b (= a % b)
m[47] = ['ADD', 52]
m[48] = ['OUT',  0]
m[49] = ['HLT',  0] # break
m[50] = [0,      0] # d
m[51] = [0,      1] # c
m[52] = [0,      5] # b
m[53] = [0,     20] # a

# decode instruction words

while True:
    instruction = m[counter][0]
    value = m[counter][1]

    if instruction == 'NOP':
        # no operation
        print(str(counter).zfill(3), ': 0000', '{0:b}'.format(value).zfill(8),
              ': NOP', str(value).zfill(3))
        counter += 1
        continue

    elif instruction == 'LDA':
        # load memory location into register A
        a = m[value][1]
        print(str(counter).zfill(3), ': 0001', '{0:b}'.format(value).zfill(8),
              ': LDA', str(value).zfill(3), ':', a, '-> A')
        counter += 1
        continue

    elif instruction == 'ADD':
        # add memory location to register A
        if a + m[value][1] < 256:
            print(str(counter).zfill(3), ': 0010', '{0:b}'.format(value).zfill(8),
                  ': ADD', str(value).zfill(3), ':', a, '+', m[value][1], '=',
                  a + m[value][1], '-> A')
            a += m[value][1]
        else:
            print(str(counter).zfill(3), ': 0010', '{0:b}'.format(value).zfill(8),
                  ': ADD', str(value).zfill(3), ':', a, '+', m[value][1], '=',
                  a + m[value][1], ';', (a + m[value][1]) % 256,
                  '-> A ; carry flag set!')
            a = (a + m[value][1]) % 256
            c = 1
        counter += 1
        continue

    elif instruction == 'SUB':
        # subtract memory location from register A
        if a - m[value][1] > -1:
            print(str(counter).zfill(3), ': 0011', '{0:b}'.format(value).zfill(8),
                  ': SUB', str(value).zfill(3), ':', a, '-', m[value][1], '=',
                  a - m[value][1], '-> A')
            a -= m[value][1]
        else:
            print(str(counter).zfill(3), ': 0011', '{0:b}'.format(value).zfill(8),
                  ': SUB', str(value).zfill(3), ':', a, '-', m[value][1], '=',
                  a - m[value][1], ';', (a - m[value][1]) % 256,
                  '-> A ; carry flag set!')
            a = (a - m[value][1]) % 256
            c = 1
        counter += 1
        continue

    elif instruction == 'STA':
        # store value in register A to memory location
        m[value][1] = a
        print(str(counter).zfill(3), ': 0100', '{0:b}'.format(value).zfill(8),
              ': STA', str(value).zfill(3), ':', a, '-> [', str(value).zfill(3),
              ']')
        counter += 1
        continue

    elif instruction == 'LDI':
        # load value into register A
        a = value
        print(str(counter).zfill(3), ': 0101', '{0:b}'.format(value).zfill(8),
              ': LDI', str(value).zfill(3), ':', a, '-> A')
        counter += 1
        continue

    elif instruction == 'JMP':
        # jump to memory location
        print(str(counter).zfill(3), ': 0110', '{0:b}'.format(value).zfill(8),
              ': JMP', str(value).zfill(3), ': -> [', str(value).zfill(3), ']')
        counter = value
        continue

    elif instruction == 'JC':
        # jump to memory location if carry flag is set
        if c == 1:
            print(str(counter).zfill(3), ': 0111', '{0:b}'.format(value).zfill(8),
                  ':  JC', str(value).zfill(3), ': -> [', str(value).zfill(3),
                  '] ; carry flag was set!')
            counter = value
            c = 0
        else:
            print(str(counter).zfill(3), ': 0111', '{0:b}'.format(value).zfill(8),
                  ':  JC', str(value).zfill(3), ': -> [',
                  str(counter + 1).zfill(3), ']')
            counter += 1
        continue

    elif instruction == 'JZ':
        # jump to memory location if value in register A = 0
        if a == 0:
            print(str(counter).zfill(3), ': 1000', '{0:b}'.format(value).zfill(8),
                  ':  JZ', str(value).zfill(3), ': -> [', str(value).zfill(3),
                  ']')
            counter = value
        else:
            print(str(counter).zfill(3), ': 1000', '{0:b}'.format(value).zfill(8),
                  ':  JZ', str(value).zfill(3), ': -> [',
                  str(counter + 1).zfill(3), ']')
            counter += 1
        continue

    elif instruction == 'OUT':
        # output value in register A
        print('\n' + str(counter).zfill(3), ': 1110', '{0:b}'.format(value).zfill(8),
              ': OUT', str(value).zfill(3), ': ->', a, '\n')
        counter += 1
        continue

    elif instruction == 'HLT':
        # halt execution
        print(str(counter).zfill(3), ': 1111', '{0:b}'.format(value).zfill(8),
              ': HLT', str(value).zfill(3))
        break
